package com.ecommerce.microcommerce.controler;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.exception.ProduitGratuitException;
import com.ecommerce.microcommerce.exception.ProduitIntrouvableException;
import com.ecommerce.microcommerce.modele.Product;
import com.ecommerce.microcommerce.modele.ProduitPlusMarge;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Gestion des produits")
//afin d'indiquer a Spring que c'est ici qu'on mettra nos methodes
//ca permet de retourner les reponses au format jason
@RestController
public class ProductControler {

	// indique qu'on a acces a une base qu'il devra instancier automatiquement
	@Autowired
	private ProductDao productDao;
	// la variable productDao aura une instance productDao automatiquement que nous
	// avons cree plutot

	@GetMapping(value = "Produits")
	public List<Product> listeProduits() {
		return productDao.findAll();

	}

	// Produits/{id}
	// appeler uniquement avec un get
	@GetMapping(value = "Produits/{id}")
	@ApiOperation(value = "Recupere un produit selon ton id")
	public Product afficherUnProduit(@PathVariable int id) throws ProduitIntrouvableException {
		// @PathVariable afin de prevenir qu'elle doit recevoir un parametre

		Product product = productDao.findById(id);

		if (product == null) {
			throw new ProduitIntrouvableException("Le produit avec l'id " + id + " n'existe pas");
		}
		
		return product;
	}

	@PostMapping(value = "Produits")
	public ResponseEntity<Void> ajouterProduit(@Valid @RequestBody Product product) throws ProduitGratuitException {
		// @ResquestBody permet d'aller chercher dans le body les informations du
		// produits et d'essayer
		// de le parser en produit.

		Product productBool = productDao.save(product);
		
		if (productBool == null) {
			return ResponseEntity.noContent().build();
		}

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(productBool.getId()).toUri();

		return ResponseEntity.created(location).build();

	}

	@GetMapping(value = "test/Produits/{prixLimit}")
	public List<Product> testeDeRequette(@PathVariable int prixLimit) {
		
		return productDao.chercherUnProduitCher(prixLimit);

	}

	@GetMapping(value = "Produits/AdminProduits")
	public List<ProduitPlusMarge> calculerMargeProduit() {
		
		List<ProduitPlusMarge> lisMarges = new ArrayList<ProduitPlusMarge>();
		List<Product> liProducts = productDao.findAll();
		for (Product product : liProducts) {
			lisMarges.add(new ProduitPlusMarge(product, product.getPrix() - product.getPrixAchat()));
		}
		return lisMarges;

	}
	
	@GetMapping(value = "Produits/tri/nom")
	public List<Product> trierProduitsParOrdreAlphabetique() {
		
		return productDao.findByOrderByNomAsc();
	}
		
	
}
