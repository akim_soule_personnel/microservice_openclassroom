package com.ecommerce.microcommerce.modele;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

//Pour ignorer plusieurs proprietes de la classe
//@JsonIgnoreProperties(value = {"id", "prixAchat"})

//permet de declarer comme entite reconnu par Jpa
@Entity
public class Product {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Length(min=3, max=20, message = "Vous essayé d'ajouter un produit dont le nom comporte plus de 20 caracteres")
	private String nom;
	
	@Min(value = 1, message = "Le prix de vente doit etre different de 0")
	private int prix;
	
	//a ne pas afficher
	@JsonIgnore
	private int prixAchat;
	
	public Product() {
		
	}
	
	public Product(int id, String nom, int prix, int prixAchat) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
		this.prixAchat = prixAchat;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getPrixAchat() {
		return prixAchat;
	}

	public void setPrixAchat(int prixAchat) {
		this.prixAchat = prixAchat;
	}
	
	@Override
	public String toString() {
		return "id=" + id + ", nom=" + nom + ", prix=" + prix + ", prixAchat=" + prixAchat;
	}
	
	
//	public String toString() {
//		return "Product {id=" + id + ", nom=" + nom + ", prix=" + prix + ", prixAchat=" + prixAchat + "} : "+getMarge();
//	}
	
	
	
	

}
