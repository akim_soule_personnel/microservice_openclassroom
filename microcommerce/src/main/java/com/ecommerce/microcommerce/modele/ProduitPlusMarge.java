package com.ecommerce.microcommerce.modele;

public class ProduitPlusMarge {
	
	Product Product;
	int marge;
	
	public ProduitPlusMarge(com.ecommerce.microcommerce.modele.Product product, int marge) {
		super();
		Product = product;
		this.marge = marge;
	}

	public Product getProduct() {
		return Product;
	}

	public void setProduct(Product product) {
		Product = product;
	}

	public int getMarge() {
		return marge;
	}

	public void setMarge(int marge) {
		this.marge = marge;
	}

	@Override
	public String toString() {
		return "ProduitPlusMarge [Product=" + Product + ", marge=" + marge + "]";
	}
	
	

	
	
	
	
	
	

}
