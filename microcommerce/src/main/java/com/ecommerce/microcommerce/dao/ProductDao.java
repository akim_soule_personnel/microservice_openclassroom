package com.ecommerce.microcommerce.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ecommerce.microcommerce.modele.Product;

//JpaRepository permet d'avoir un certain nombre de methodes predefinies
@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {
 
	Product findById(int id);
	
	List<Product> findByPrixGreaterThan(int prixLimit);
	
	//@Query("select new com.example.IdsOnly(t.id, t.otherId) from TestTable t where t.creationDate > ?1 and t.type in (?2)")
	@Query("Select new com.ecommerce.microcommerce.modele.Product(p.id, p.nom, p.prix, p.prixAchat) FROM Product p where p.prix > ?1")
	List<Product>chercherUnProduitCher(int prix);
	
	//@Query("Select new com.ecommerce.microcommerce.modele.Product(p.id, p.nom, p.prix, p.prixAchat) FROM Product order by p.nom")
	List<Product>findByOrderByNomAsc();
	
}
